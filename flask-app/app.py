from flask import Flask, render_template
import random

app = Flask(__name__)

# list of dog images
images = [
    "https://68.media.tumblr.com/da8eaedab64a670ea438c20201b5584c/tumblr_nxpgs2zQjw1tyncywo1_400.gif",
    "http://25.media.tumblr.com/c1ede02848e27a52bab3e0943f1f732b/tumblr_mth9t4tw5K1shpkbpo1_400.gif",
    "http://31.media.tumblr.com/2e67acd4f4b57c64c733a28d891c9644/tumblr_nggw7mIYes1te5ruso1_r1_250.gif",
    "http://25.media.tumblr.com/8f4b1f5255e051173acb774444fdc761/tumblr_mw96bk8lLk1sfmtaxo1_500.gif",
    "http://24.media.tumblr.com/a32a61cb3e1750cf5db59c9a8c90e800/tumblr_n6xgyzHPq31td9my6o1_500.gif",
    "http://24.media.tumblr.com/tumblr_m64aku44wC1rynz9jo1_500.gif",
    "http://68.media.tumblr.com/31e7168721befdf3747abfeb8c597786/tumblr_o7hab7lcGU1v8uqeqo1_400.gif",
    "http://24.media.tumblr.com/16a98ae27367593d459898304ba1c5af/tumblr_mqfjpgh2681qaru4bo1_400.gif",
    "http://68.media.tumblr.com/949f905054bb9c7711ee6433cc771665/tumblr_ojcvuoRr3z1vhqe9po1_500.gif",
    "http://49.media.tumblr.com/f1e9db10ec17a9ad30e702b815f52325/tumblr_o5j8wdYHCi1v8uqeqo1_500.gif",
    "http://24.media.tumblr.com/6cbd46a42bf3a4aad6ec3984aa91e617/tumblr_n6dbbtT27R1rynv7io1_400.gif",
]

@app.route('/dogs')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
